<?php

$searchRoot = 'test_search';
$searchName = 'test.txt';
$searchResult = [];

function recursiveFileSearch($searchRoot, $searchName, &$searchResult) {
    $files = scandir($searchRoot);
    foreach ($files as $file) {
        if ($file == '.' || $file == '..') {
            continue;
        }
        $fullPath = $searchRoot . '/' . $file;
        if (is_dir($fullPath)) {
            recursiveFileSearch($fullPath, $searchName, $searchResult);
        } elseif ($file == $searchName) {
            $searchResult[] = $fullPath;
        }
    }
}

recursiveFileSearch($searchRoot, $searchName, $searchResult);

function notEmptyFile ($filepath) {
    return filesize($filepath) != 0;
}
$tempArray = array_filter($searchResult, 'notEmptyFile');

if (empty($tempArray)) {
    echo "No results found";
} else {
    echo "Search results:\n";
    foreach ($tempArray as $result) {
        echo "$result\n";
    }
}